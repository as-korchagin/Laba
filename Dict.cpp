//
// Created by andrey on 29.10.16.
//

#include <iostream>
#include <cstring>
#include "Dict.h"

using namespace std;

Dict::Dict() {
//    cout << "Dict constructor " << &keys << " and " << &values << endl;
//    incSize();
//    keys = new char[length];
//    values = new int [length];
}

void Dict::incSize() {
    length++;
}

Dict::~Dict() {
//    cout << "Деструктор Dict. Не делает ничего" << endl;
//    cout << "Dict destructor " << &keys << " and " << &values << endl;
//    delete[] keys;
//    delete[] values;
}

int Dict::getSize() {
    return length;
}

void Dict::append(char key, int value) {
    char *temp_keys;
    int *temp_values;
    if (existingKey(key)) {
        incValue(key);
    } else {
        if (length != 0) {
            temp_keys = new char[length];
            temp_values = new int[length];
            for (int i = 0; i < length; i++) {
                temp_keys[i] = keys[i];
                temp_values[i] = values[i];
            }
            delete[] keys;
            delete[] values;
        }
        incSize();
        keys = new char[length];
        values = new int[length];
//        cout << "Append Dict. память выделена: " << &keys << ", " << &values<< endl;
        memory_allocated = true;
        if (length != 1) {
            for (int i = 0; i < length - 1; i++) {
                keys[i] = temp_keys[i];
                values[i] = temp_values[i];
            }
            delete[] temp_keys;
            delete[] temp_values;
        }
        keys[length - 1] = key;
        values[length - 1] = value;
    }


}

int Dict::getValue(char key) {
    int i = 0;

    while (keys[i] != key && i < length) {
        i++;
    }

    return values[i];
}

bool Dict::existingKey(char key) {
    int i = 0;
    bool match = false;
    if (memory_allocated) {
        while (keys[i] != key && i < length) {
            i++;
        }
        if (keys[i] == key)
            match = true;
    }
    return match;
}

void Dict::incValue(char key) {
    int i = 0;
    while (keys[i] != key && i < length) {
        i++;
    }
    values[i]++;
}

void Dict::flush() {
//    cout << "Flush Dict. память освобождена: " << &keys << ", " << &values<< endl;
    length = 0;
    memory_allocated = false;
    delete[] keys;
    delete[] values;
}

char *Dict::getKeys() {
    if (memory_allocated)
        return keys;
    else
        return nullptr;
}

bool Dict::memoryAllocated() {
    return memory_allocated;
}

void Dict::operator=(Dict &rValue) {
    strcpy(keys, rValue.keys);
    for (int i = 0; i < length; i++) {
        values[i] = rValue.values[i];
    }
}

void Dict::init(int size) {
    length = size;
    memory_allocated = true;
    keys = new char[size];
    values = new int[size];
//    cout << "Init Dict. память выделена: " << &keys << ", " << &values<< endl;
}
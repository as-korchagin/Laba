
#pragma once
#define N 100

#include <iostream>

using namespace std;

class Planet {
private:
    static int count;
    char *_name;
    unsigned int _diameter;
    bool _life;
    int _satellites;
public:

    Planet();

    ~Planet();

    Planet(Planet const &);

    void setName(char *);

    void setDiameter(unsigned int);

    void setLife(bool);

    void setSatellites(int);

    bool getLife();

    char *getName();

    int getSatellites();

    int getDiameter();

    void makeEmpty();

    void operator=(Planet const &);

};

ostream &operator<<(ostream &, Planet &);

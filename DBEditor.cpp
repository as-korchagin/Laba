
//
// Created by andrey on 04.10.16.
//
#include "DBEditor.h"


using namespace std;

void DBEditor::getPlanets(Planet *planets) {
    int satellites;
    unsigned int diameter;
    bool life;
    char buff[20];
    planet_counter = 0;
    ifstream fin(filename);
    fin >> planet_counter;
    for (int i = 0; i < planet_counter; i++) {
//        fin >> planets[i]._name >> planets[i]._diameter >> planets[i]._life >> planets[i]._satellites;
        fin >> buff >> diameter >> life >> satellites;
        planets[i].setName(buff);
        planets[i].setDiameter(diameter);
        planets[i].setLife(life);
        planets[i].setSatellites(satellites);
    }
    fin.close();
}

fstream &operator<<(fstream &s, Planet &planet) {
    s << planet.getName() << ' ' << planet.getDiameter() << ' ' << planet.getLife() << ' ' << planet.getSatellites()
      << '\n';
    return s;
}

void DBEditor::change_db(Planet *planet, Planet *planets) {
    getPlanets(planets);
    incPlanetCounter();
    planets[getSize() - 1] = *planet;
//    for (int i = 0; i < getSize(); i++)
//        cout << &planets[i] << "   " << planets[i];
    write_to_db(planets, getSize());
}

void DBEditor::write_to_db(Planet *planets, int planets_size) {
    fstream fout(filename, ios_base::out);
    if (!fout) {
        cout << "Ошибка открытия файла" << endl;
    } else {
        fout << planets_size << '\n';
        for (int i = 0; i < planets_size; i++) {
            fout << planets[i];
        }
    }
    fout.close();
}

DBEditor::DBEditor() {
//    cout << "Конструктор DBEditor" << endl;
}

DBEditor::~DBEditor() {
//    cout << "Деструктор DBEditor" << endl;
}
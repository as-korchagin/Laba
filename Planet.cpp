
#include <cstring>
#include "Planet.h"


using namespace std;

int Planet::count = 0;

Planet::Planet() {
    _name = new char[N];
    count++;
    cout << count << endl;
}

Planet::Planet(Planet const &p) {
    cout << ++count << endl;
    _name = new char[strlen(p._name) + 1];
    strcpy(_name, p._name);
    _diameter = p._diameter;
    _life = p._life;
    _satellites = p._satellites;
}


void Planet::operator=(Planet const &copy_planet) {
    setName(copy_planet._name);
    setDiameter(copy_planet._diameter);
    setLife(copy_planet._life);
    setSatellites(copy_planet._satellites);
}

void Planet::setName(char *name) {
    delete[] _name;
    _name = new char[strlen(name) + 1];
    strcpy(_name, name);
}

void Planet::setDiameter(unsigned int diameter) {
    _diameter = diameter;
}

void Planet::setLife(bool life) {
    _life = life;
}

void Planet::setSatellites(int satellites) {
    _satellites = satellites;
}

bool Planet::getLife() {
    return _life;
}

char *Planet::getName() {
    return _name;
}


Planet::~Planet() {
    --count;
    cout << count << endl;
    delete[] _name;
}

int Planet::getSatellites() {
    return _satellites;
}

int Planet::getDiameter() {
    return _diameter;
}

void Planet::makeEmpty() {
    delete[] _name;
    _name = new char[N];
    _diameter = 0;
    _life = 0;
    _satellites = 0;
}

ostream &operator<<(ostream &s, Planet &planet) {
    s << "Имя: " << planet.getName() << "; "
      << "Диаметр: " << planet.getDiameter() << "; "
      << "Жизнь: " << planet.getLife() << "; "
      << "Спутники: " << planet.getSatellites()
      << '\n';
    return s;
}
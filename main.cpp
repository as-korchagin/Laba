
#pragma once
#define N 100

#include "Planet.h"
#include "DBEditor.h"

using namespace std;


int menu();

void add_planet(int);

void change_planet();

void remove_planet();

int list_db();

void search_planet();

void sort_planets();

void swap_planets(int, int);

Planet *planets;

int planet_count = 0;

Planet f(Planet &pl) {
    Planet planet(pl);
    return planet;
}

int main() {
    DBEditor editor;
    planet_count = editor.getSize();
    planets = new Planet[planet_count];
    editor.getPlanets(planets);
//    remove_planet();
//    menu();
    Planet p = f(planets[1]);
    cout << p;
    delete[] planets;
    return 0;
}

int menu() {
    int operation;
    cout << endl << endl << endl;
    cout << " ============== ГЛАВНОЕ МЕНЮ ======================\n";
    cout << "1 - Добавление планеты\t 4 - Корректировка сведений" << endl;
    cout << "2 - Удаление планеты\t 5 - Вывод базы на экран" << endl;
    cout << "3 - Поиск планеты\t 6 - Отсортировать список" << endl;
    cout << "\t\t\t 7 - выход" << endl << endl << endl;
    cout << "Для выбора операции введите цифру от 1 до 7 :";
    cin >> operation;
    cout << endl << endl << endl;
    switch (operation) {
        case 1: {
            // TODO: Add DBEditor.getSize() and remove from add_planet
            cout << "Добавление планеты: \n";
            add_planet(planet_count);
            break;
        }
        case 2: {
            cout << "Удаление планеты: \n";
            remove_planet();
            break;
        }
        case 3: {
            cout << "Поиск планеты \n";
            search_planet();
            break;
        }
        case 4: {
            cout << "Изменить сведения \n";
            change_planet();
            break;
        }
        case 5: {
            cout << "Вывод базы данных на экран \n";
            list_db();
            break;
        }
        case 6: {
            cout << "Отсортировать список \n";
            sort_planets();
            break;
        }
        case 7: {
            cout << "Выход" << endl;
            break;
        }
        default: {
            cout << "Нет такого пункта, попробуйте еще раз" << endl;
            menu();
            break;
        }
    }
    return operation;
}

void add_planet(int planet_counter) {
    Planet planet;
    delete[] planets;
    planets = new Planet[planet_counter + 1];
    char *name = new char[N];
    int diameter, satellites;
    bool life;
    cout << "Введите название планеты: ";
    cin >> name;
    planet.setName(name);
    delete[] name;
    cout << "Введите диаметр планеты: ";
    cin >> diameter;
    planet.setDiameter(diameter);
    cout << "Есть ли на планете жизнь? (1/0): ";
    cin >> life;
    planet.setLife(life);
    cout << "Введите количество спутников у планеты: ";
    cin >> satellites;
    planet.setSatellites(satellites);
    DBEditor editor;
    editor.change_db(&planet, planets);
}


int list_db() {
    DBEditor editor;
    editor.getPlanets(planets);
    int counter = editor.getSize();
    cout << "Прочитано " << counter << " планет:" << endl;
    for (int i = 0; i < counter; i++) {
        cout << i + 1 << ". " << planets[i];
    }
    return counter;
}

void remove_planet() {
    int arr_size = list_db();
    DBEditor editor;
    int planet_to_remove;
    cout << endl << "Введите номер удаляемой планеты: ";
    cin >> planet_to_remove;
    for (int i = planet_to_remove - 1; i < arr_size - 1; i++) {
        planets[i] = planets[i + 1];
    }
    planets[arr_size - 1].makeEmpty();
    editor.decPlanetCounter();
    editor.write_to_db(planets, arr_size - 1);
    cout << "Планета " << planet_to_remove << " удалена." << endl;

}


void change_planet() {
    DBEditor editor;
    int planet_counter = list_db();
    int planet_to_change;
    int propery_to_change;
    cout << "Ввыедите планету для изменения: ";
    cin >> planet_to_change;
    cout << endl << "Какое именно свойство вы хотите поменять?" << endl << "1. Название" << endl << "2. Диаметр" << endl
         << "3. Наличие жизни" << endl << "4. Количесво спутников" << endl;
    cin >> propery_to_change;
    switch (propery_to_change) {
        case 1:
            cout << "Введите новое название: ";
            char name[N];
            cin >> name;
            planets[planet_to_change - 1].setName(name);
            editor.write_to_db(planets, planet_counter);
            break;
        case 2:
            cout << "Введите новый диаметр: ";
            int diameter;
            cin >> diameter;
            planets[planet_to_change - 1].setDiameter(diameter);
            editor.write_to_db(planets, planet_counter);
            break;
        case 3:
            cout << planets[planet_to_change - 1] << endl;
            if (planets[planet_to_change - 1].getLife() == 1) {
                planets[planet_to_change - 1].setLife(0);
            } else {
                planets[
                        planet_to_change - 1].setLife(1);
            }
            editor.write_to_db(planets, planet_counter);
            break;
        case 4:
            cout << "Введите новое количество спутников: ";
            int satellites;
            cin >> satellites;
            planets[planet_to_change - 1].setSatellites(satellites);
            editor.write_to_db(planets, planet_counter);
            break;
        default:
            cout << "Нет такого свойства";
            exit(0);
    }

}

void search_planet() {
    DBEditor editor;
    editor.getPlanets(planets);
    cout << "Введите название планеты, которую вы хотите найти: ";
    char somename[100];
    char *name_from_db;
    bool full_word = false;
    cin >> somename;
    int i = 0;
    for (i = 0; i < editor.getSize(); i++) {
        int j = 0;
        full_word = true;
        name_from_db = planets[i].getName();
        while (somename[j] != '\0') {
            if (somename[j] == *name_from_db) {
                name_from_db++;
                j++;
                full_word = true;
                continue;
            } else {
                full_word = false;
                break;
            }
        }
        if (full_word)
            break;
    }

    if (full_word)
        cout << planets[i];
    else
        cout << "Нет такой планеты";
}


void sort_planets() {
    DBEditor editor;
    editor.getPlanets(planets);
    cout << "По каком признаку вы хотите отсортировать список?\n1. По имени\n2. По диаметру\n3. По наличию жизни\n"
         << "4. По количеству спутников\n";
    int choice;
    int db_size = editor.getSize();
    cin >> choice;
    char *first_planet;
    char *second_planet;
//    Dict dict;
//    Dict temp_dict;
//    Dict previous_dict;
    switch (choice) {
        case 1: {
            bool swapped;
            for (int i = 0; i < planet_count - 1; i++) {
                swapped = false;
                for (int j = 0; j < planet_count - 1 - i; j++) {
                    first_planet = planets[j].getName();
                    second_planet = planets[j + 1].getName();
                    if ((int) *first_planet > (int) *second_planet) {
                        swap_planets(j, j + 1);
                        swapped = true;
                        editor.write_to_db(planets, editor.getSize());
                    }

                }
                if (!swapped)
                    break;

            }
//            for (int i = 0; i < planet_count - 1; i++) {
//                if ((int) *(planets[i].getName()) == (int) *(planets[i + 1].getName())) {
//                    if (dict.existingKey(*(planets[i].getName()))) {
//                        dict.incValue(*(planets[i]).getName());
//                    } else {
//                        dict.append(*(planets[i].getName()), 2);
//                    }
//                }
//            }



//            for (int i = 0; i < dict.getSize(); i++) {
//                cout << dict.getKeys()[i] << ' ' << dict.getValue(dict.getKeys()[i]) << endl;
//            }
//            int letter = 0;
//            bool repeat = true;
//            int iterator = 0;
//            while (repeat) {
//                repeat = false;
//                int entry = 0;
//                for (int i = 0; i < dict.getSize(); i++) {
//                    int temp = 0;
//                    char key = *(dict.getKeys() + i);
//                    entry = dict.getValue(key);
//                    cout << *(planets[iterator].getName() + letter) << endl;
//                    while (*(planets[iterator].getName() + letter) != key && in_previous_dict(previous_dict, *(planets[iterator].getName() + letter))) {
//                        iterator++;
//                        cout << *(planets[iterator].getName() + letter) << endl;
//                    }
//                    for (int j = iterator; j < iterator + entry - 1; j++) {
//                        for (int k = iterator; k < iterator + entry - temp - 1; k++) {
//                            if ((int) *(planets[k].getName() + letter + 1) >
//                                (int) *(planets[k + 1].getName() + letter + 1)) {
//                                cout << "planets to swap: " << endl << planets[k] << planets[k + 1];
//                                swap_planets(k, k + 1);
//                                cout << "swapped planets: " << endl << planets[k] << planets[k + 1];
//                            }
//                        }
//                        temp++;
//                    }
//                    letter++;
//                    dict.flush();
//                    for (int k = iterator; k < iterator + entry; k++) {
//                        if (dict.existingKey(*(planets[k].getName() + letter))) {
//                            dict.incValue(*(planets[k].getName() + letter));
//                        } else {
//                            dict.append(*(planets[k].getName() + letter), 1);
//                        }
//                    }
//                    for (int z = 0; z < dict.getSize(); z++) {
//                        if (dict.getValue(dict.getKeys()[z]) > 1) {
//                            repeat = true;
//                            temp_dict.append(dict.getKeys()[z], dict.getValue(dict.getKeys()[z]));
//                        }
//                    }
//                    if (repeat) {
//                        previous_dict.flush();
//                        previous_dict.init(dict.getSize());
//                        previous_dict = dict;
//                        dict.flush();
//                        dict.init(temp_dict.getSize());
//                        dict = temp_dict;
//                        temp_dict.flush();
//                    }
//                }
//                iterator = 0;
//            }
//            previous_dict.flush();
//            for (int y = 0; y < planet_count; y++) {
//                cout << planets[y];
//            }
//            editor.write_to_db(planets, editor.getSize());
            break;
        }
        case 2: {
            for (int i = 0; i < db_size - 1; i++) {
                for (int j = 0; j < db_size - 1 - i; j++) {
                    if (planets[j + 1].getDiameter() < planets[j].getDiameter()) {
                        swap_planets(j, j + 1);
                    }
                }
            }
            break;
        }
        case 3: {
            for (int i = 0; i < db_size - 1; i++) {
                for (int j = 0; j < db_size - 1 - i; j++) {
                    if (planets[j + 1].getLife() < planets[j].getLife()) {
                        swap_planets(j, j + 1);
                    }
                }
            }
            break;
        }
        case 4: {
            for (int i = 0; i < db_size - 1; i++) {
                for (int j = 0; j < db_size - 1 - i; j++) {
                    if (planets[j + 1].getSatellites() < planets[j].getSatellites()) {
                        swap_planets(j, j + 1);
                    }
                }
            }
            break;
        }
        default: {
            cout << "Нет такого варианта" << endl;
            exit(0);
        }
    }
    for (int i = 0; i < planet_count; i++) {
        cout << i + 1 << ". " << planets[i];
    }
    cout << "Записать измениения в базу данных? (0/1): ";
    cin >> choice;
    if (choice)
        editor.write_to_db(planets, editor.getSize());

}

void swap_planets(int first_planet, int second_planet) {
    Planet temp_planet;
    temp_planet = planets[first_planet];
    planets[first_planet] = planets[second_planet];
    planets[second_planet] = temp_planet;

}


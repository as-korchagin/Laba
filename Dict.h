//
// Created by andrey on 29.10.16.
//

#ifndef LABA_DICT_H
#define LABA_DICT_H


class Dict {
private:
    char *keys;
    int *values;
    int length = 0;
    bool memory_allocated = false;

public:
    char *getKeys();

    void append(char, int);

    void flush();

    Dict();

    ~Dict();

    int getSize();

    void incSize();

    int getValue(char);

    bool existingKey(char);

    void incValue(char);

    bool memoryAllocated();

    void operator=(Dict &);

    void init(int);

};


#endif //LABA_DICT_H

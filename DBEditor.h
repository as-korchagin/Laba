
//
// Created by andrey on 04.10.16.
//

#include <fstream>
#include <cstring>
#include "Planet.h"

#define N 100


class DBEditor {
private:
    char *filename = "DB.txt";
    char *tempFileName = "DBtemp.txt";
    int planet_counter;
public:

    DBEditor();

    ~DBEditor();

    void change_db(Planet *, Planet *);

    void write_to_db(Planet*, int);

    void getPlanets(Planet *);

    int getSize(){
        int temp_counter = 0;
        ifstream fin(filename);
        fin >> temp_counter;
        fin.close();
        if (temp_counter > planet_counter){
            planet_counter = temp_counter;
        }
        return planet_counter;
    };

    void incPlanetCounter() {
        ++planet_counter;
    };

    void decPlanetCounter(){
        --planet_counter;
    }

};

